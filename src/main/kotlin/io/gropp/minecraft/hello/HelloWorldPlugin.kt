package io.gropp.minecraft.hello

import org.bukkit.plugin.java.JavaPlugin

@Suppress("unused") // plugin main class
class HelloWorldPlugin : JavaPlugin() {
    override fun onEnable() {
        logger.info("Hello World!")

        requireNotNull(getCommand(HELLO_COMMAND_NAME)).setExecutor(HelloWorldCommandExecutor())
    }

    companion object {
        private const val HELLO_COMMAND_NAME = "hello"
    }
}
