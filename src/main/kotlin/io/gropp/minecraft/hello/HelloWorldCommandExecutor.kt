package io.gropp.minecraft.hello

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class HelloWorldCommandExecutor : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean =
        when (sender) {
            is Player -> sayHello(sender)
            else -> false
        }

    private fun sayHello(player: Player): Boolean {
        player.sendMessage("Hello ${player.name}!")
        return true
    }
}
