Hello World
===========

A Kotlin test plugin for Minecraft / Spigot.

Building
--------

Run
```
./gradlew shadowJar 
```
or
```
gradlew.bat shadowJar
```

This creates a jar file `hello-minecraft-plugin-1.0.0-SNAPSHOT-all.jar`
in `build/libs`. Copy or symlink this file to the `plugins` directory of
your Spigot installation.


Usage
-----

On server startup, you should see a "Hello World!" message in the server log.

In the game, you can use the command `/hello` to receive a chat message.
