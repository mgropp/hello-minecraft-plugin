plugins {
    kotlin("jvm") version "1.8.0"
    id("com.github.johnrengelman.shadow").version("7.0.0")
    id("com.ncorti.ktfmt.gradle") version "0.10.0"
}

group = "io.gropp"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
}

dependencies {
    implementation("org.spigotmc:spigot-api:1.19.2-R0.1-SNAPSHOT")
    constraints {
        implementation("org.yaml:snakeyaml:2.0")
    }

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}

ktfmt {
    kotlinLangStyle()
    maxWidth.set(120)
}